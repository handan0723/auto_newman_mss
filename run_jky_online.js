const newman = require('/usr/local/lib/node_modules/newman');

newman.run({
    collection: './mss_collection_full.json', 
    environment:'./mss_environment_online.json',
    reporters: ['cli', 'htmlextra'], 
    insecure:true,
    iterationCount: 1,
    reporter: {
        htmlextra : {
            export: './report/index.html'
        } 
    }
}, function(err, summary){
    if (err) {
        throw err;
    }

    const { total, pending, failed } = summary.run.stats.assertions;
    const { responseAverage, responseMin, responseMax, started, completed } = summary.run.timings;

    if(summary.run.failures && summary.run.failures.length>0){
        console.log("！！！Auto test Failed！！！");
        process.exit(1);
    }
    else{
        console.log("！！！Auto test Success！！！");
    }
});
